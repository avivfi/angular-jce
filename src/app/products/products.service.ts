import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';


@Injectable()
export class ProductsService {

productsObservabale;

//getProducts(){
//return this._http.get(this._url).map(res =>res.json()).delay(2000);
//this.productsObservabale = this.af.database.list('/product');
//return this.productsObservabale;
//}

getProducts(){
    this.productsObservabale = this.af.database.list('/product').map(
    products =>{
      products.map(
        product => {
          product.productsTitle =[];
          for(var u in product.categoryId){
            product.productsTitle.push(
              this.af.database.object('/category/' + u)
            )
          } }
      );
      return products;}
      );
  return this.productsObservabale;
  }


deleteProduct(product){
     let productKey = product.$key;
    this.af.database.object('/product/' + productKey).remove();
  }

 constructor(private af:AngularFire) { }


}
