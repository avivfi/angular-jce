import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.Service';


@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css'],
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #FF9999	; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f8;
         border-color: #ecf0f7; 
         color: #FF9999;
    }     
  `]
})
export class InvoicesComponent implements OnInit {

invoices;

addInvoice(invoice){
    this._invoicesService.addInvoice(invoice);
    
  }

//addInvoice(invoice){this.invoices.push(invoice);
// }

 constructor(private _invoicesService:InvoicesService) { }

  ngOnInit() {
  this._invoicesService.getInvoices().subscribe(invoicesData =>{  
  this.invoices= invoicesData;
  console.log(this.invoices)});
}

}
