import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';

@Injectable()
export class InvoicesService {

invoicesObservable;

getInvoices(){
this.invoicesObservable = this.af.database.list('/invoices');
return this.invoicesObservable;
}

addInvoice(invoice){
  this.invoicesObservable.push(invoice);
}

constructor(private af:AngularFire) { }


}
