import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import {UsersService} from './users/users.Service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import{AngularFireModule} from 'angularfire2';
import { ProductComponent } from './product/product.component';
import {ProductService} from './product/product.Service';
import { ProductsComponent } from './products/products.component';
import {ProductsService} from './products/products.Service';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';
import {InvoicesService} from './invoices/invoices.Service';


export const firebaseConfig = {
    apiKey: "AIzaSyA6cP-7pdq95kmuBuP5eyNF19O7sv4OALA",
    authDomain: "angular-97105.firebaseapp.com",
    databaseURL: "https://angular-97105.firebaseio.com",
    storageBucket: "angular-97105.appspot.com",
    messagingSenderId: "304961583981"
}

const appRoutes:Routes= [

  {path:'users',component:UsersComponent},
  {path:'posts',component:PostsComponent},
  {path:'products',component:ProductsComponent},
  {path:'invoices',component:InvoiceFormComponent},
  {path:'invoiceForm',component:InvoicesComponent},
  {path:'',component:UsersComponent},
  {path:'**',component:PageNotFoundComponent}

]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    ProductComponent,
    ProductsComponent,
    InvoiceFormComponent,
    InvoicesComponent,
    InvoiceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService,ProductsService,InvoicesService,ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
