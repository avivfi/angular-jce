import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { Invoice } from './invoice';

@Component({
  selector: 'jce-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],

})
export class InvoiceComponent implements OnInit {

invoice:Invoice;

  constructor() { }

  ngOnInit() {
  }

}
