import { AnprojectPage } from './app.po';

describe('anproject App', function() {
  let page: AnprojectPage;

  beforeEach(() => {
    page = new AnprojectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
